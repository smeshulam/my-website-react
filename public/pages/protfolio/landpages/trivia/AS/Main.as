﻿/**
 * ...
 * @author shahar meshulam
 */
import AS.GlobalVars


 
class AS.Main extends MovieClip
{
	public var startGame_BTN:MovieClip;
	public var instructions_BTN:MovieClip;
	public var home_BTN:MovieClip;
	 
	
	public function Main() 
	{
		GlobalVars.main = this;
		start_BTNS();
		
	}
	
	
	// set action for start game BTNS
	
	function start_BTNS() {

		GlobalVars.main.startGame_BTN.onRelease = function() {
				GlobalVars.main.gotoAndStop("game");
				
				GlobalVars.main.home_BTN.onRelease = function() {
				GlobalVars.main.gotoAndStop("home");
				GlobalVars.main.start_BTNS();
				}	
				}
				
		GlobalVars.main.instructions_BTN.onRelease = function() {
				GlobalVars.main.gotoAndStop("instructions");
				
				GlobalVars.main.home_BTN.onRelease = function() {
				GlobalVars.main.gotoAndStop("home");
				GlobalVars.main.start_BTNS();
				}	
				}	
					
		
		}
	
}