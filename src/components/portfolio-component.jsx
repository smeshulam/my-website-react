import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
//import './GetOnlinePosts.css'
class Portfolio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error : null,
            isLoaded : false,
            category: false,
            portfolio : []          
        };
    }
    componentDidMount() {
        // fetch portfolio json
        fetch(`${process.env.PUBLIC_URL}/portfolio.json`)
        .then(response => response.json())
        .then(
            // handle the result
            (result) => {
                this.setState({
                    isLoaded : true,
                    portfolio : result
                });
            },
            // Handle error 
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            },
        )
    }
    getCategoryItems(categoryName) {
        var items= this.state.portfolio.categories.find(function(item){
            return item.name === categoryName;         
        });
        return items;
      }
    render() {
        const {error, isLoaded, portfolio} = this.state;
        const {match} = this.props;
        if(error){
            return <div>Error in loading portfolio</div>
        }else if (!isLoaded) {
            return <div>Loading ...</div>
        }else{
            return(
                <div>
                    <ul className="item">
                    {
                        portfolio.categories.map((data, i) => (
                            <li key={i} align="start">
                                <div>
                                <Link to={`${match.url}/${data.name}`}>
                                    <p className="title">{data.name}</p>
                                </Link>
                                </div>
                            </li>
                        ))
                    }
                    </ul>
                    <Route path={`${match.path}/:name`} render= {({match}) => ( 
                            <div> <h3> {match.params.name} </h3>
                            {
                            this.getCategoryItems(match.params.name).items.map((item, i) => (
                                <div key={i}>
                                    <h1>{item.item_name}</h1>
                                    <img src={process.env.PUBLIC_URL + '/' + item.thumb_src} />
                                    <a href={item.src} target="_blank">link</a>
                                </div>
                            ))}
                            </div>
                        )}/>
                </div>
            );
        }
      
    }
  }
  
  export default Portfolio;