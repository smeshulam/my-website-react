﻿var clickTimerSpeed = 200;
var progFromChooseBtn = false;
var deviceFromChooseBtn = false;
var SHOWDEVICES = true;

$(document).ready(function () {

    jQuery("#personalId").keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

//footer box click
    $('.chooseBTN').live("click", function () {
        var curObj = $(this);
        var elm = $(this).closest(".contenSlid").find(".packageBTN");
        if ($(elm).attr('class') != undefined) {
            progFromChooseBtn = true;
        }
        $(this).closest(".contenSlid").find(".packageBTN").click();
        $(this).parent('LI').addClass('selected');
        $(this).attr('class', 'packageBTN');
        $(this).closest(".contenSlid").prev('h2').append('<span class="selectText">' + $(this).closest('LI').find(".detailsText").val() + '</span><a href="javascript:void(0);" title="" class="vIcon">סגור והמשך</a>');
        var clickTimer = setTimeout(function () {
            curObj.closest(".contenSlid").prev('h2').find(".vIcon").trigger("click");
        }, clickTimerSpeed);
    });

    //        $(".slideContentTitle").click(function ()
    //            if ($(".vIcon", $(this)).length > 0) {
    //                $(".vIcon", $(this)).click();
    //            }
    //        });

    //$(".slideContentTitle").click(function () {
    $(".vIcon").live("click", function () {

        var curObj = $(this);
        if (curObj.length > 0) {
            if (curObj.hasClass("upd")) {
                if ($(".contenSlid:visible").length > 0) {

                    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {

                        var ieversion = new Number(RegExp.$1);
                        if (ieversion < 8) {
                            $(".contenSlid:visible").hide(0);
                            curObj.closest("h2").next(".contenSlid").show(0);
                            $("#mcs_container").mCustomScrollbar("horizontal", 0, "easeOutCirc", 1, "fixed", "no", "no", 20);
                        }
                        else {
                            $(".contenSlid:visible").slideUp(200);
                            curObj.closest("h2").next(".contenSlid").slideDown(200);
                        }
                    }
                    else {
                        $(".contenSlid:visible").slideUp(200);
                        curObj.closest("h2").next(".contenSlid").slideDown(200);
                    }
                }
                else {

                    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {

                        var ieversion = new Number(RegExp.$1);
                        if (ieversion < 8) {
                            curObj.closest("h2").next(".contenSlid").show(0);
                            $("#mcs_container").mCustomScrollbar("horizontal", 0, "easeOutCirc", 1, "fixed", "no", "no", 20);
                        }
                        else {
                            curObj.closest("h2").next(".contenSlid").slideDown(200);
                        }
                    }
                    else {
                        curObj.closest("h2").next(".contenSlid").slideDown(200);
                    }
                }
                $(".vIcon").each(function () {
                    $(this).text("עדכן");
                    $(this).addClass("upd");
                });

                curObj.text("סגור והמשך");
                curObj.removeClass("upd");
                if (curObj.closest("h2").next(".contenSlid").find(".nextBTN").length > 0) {
                    $(".vIcon").each(function () {
                        if ($(this).offset().top > curObj.offset().top) {
                            $(this).hide();
                        }
                    });

                }
            }
            else {
                if (curObj.closest("h2").next(".contenSlid").find(".nextBTN").length > 0) {
                    curObj.closest("h2").next(".contenSlid").find(".nextBTN").click();

                }
                else
                if (curObj.closest("h2").next(".contenSlid").is(":visible")) {
                    curInd = $(".contenSlid").index($(".contenSlid:visible"));


                    if ($(".contenSlid:eq(" + (curInd + 1) + ")").prev("H2").find(".vIcon").hasClass("upd")) {
                        curInd++;
                    }
                    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                        var ieversion = new Number(RegExp.$1);
                        if (ieversion < 8) {
                            $(".contenSlid:visible").hide(0);
                            $(".contenSlid:eq(" + (curInd + 1) + ")").show(0);
                            showNumbers(curInd);
                            $("#mcs_container").mCustomScrollbar("horizontal", 0, "easeOutCirc", 1, "fixed", "no", "no", 20);
                        }
                        else {
                            $(".contenSlid:visible").slideUp(500, showNumbers(curInd));
                            $(".contenSlid:eq(" + (curInd + 1) + ")").slideDown(200);
                        }
                    }
                    else {
                        $(".contenSlid:visible").slideUp(500, showNumbers(curInd));
                        $(".contenSlid:eq(" + (curInd + 1) + ")").slideDown(200);
                    }
                }
                curObj.text("עדכן");
                curObj.addClass("upd");
            }
        }
        else {
            curInd = $(".contenSlid").index($(".contenSlid:visible"));
            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                var ieversion = new Number(RegExp.$1);
                if (ieversion < 8) {
                    $(".contenSlid:visible").hide(0);
                    $(this).next(".contenSlid").slideDown(0);
                    $("#mcs_container").mCustomScrollbar("horizontal", 0, "easeOutCirc", 1, "fixed", "no", "no", 20);
                }
                else {
                    $(".contenSlid:visible").hide(200);
                    $(this).next(".contenSlid").slideDown(200);
                }
            }
            else {
                $(".contenSlid:visible").hide(200);
                $(this).next(".contenSlid").slideDown(200);
            }
        }
    });

    $(".packageBTN").live("click", function () {

        if (progFromChooseBtn) {
            $(this).parent('LI').removeClass('selected');
            $(this).attr('class', 'chooseBTN btn33');
            $(this).closest(".contenSlid").prev('h2').find(".selectText").remove();
            $(this).closest(".contenSlid").prev('h2').find(".vIcon").remove();
            progFromChooseBtn = false;
        }

    });

//    $(".vIcon").live("click", function () {
//        //            var curObj = $(this);
//        //            if ($(this).hasClass("upd")) {
//        //                if ($(".contenSlid:visible").length > 0) {
//
//        //                    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
//
//        //                        var ieversion = new Number(RegExp.$1);
//        //                        if (ieversion < 8) {
//        //                            $(".contenSlid:visible").hide(0);
//        //                            curObj.closest("h2").next(".contenSlid").show(0);
//        //                            $("#mcs_container").mCustomScrollbar("horizontal", 0, "easeOutCirc", 1, "fixed", "no", "no", 20);
//        //                        }
//        //                        else {
//        //                            $(".contenSlid:visible").slideUp(200);
//        //                            curObj.closest("h2").next(".contenSlid").slideDown(200);
//        //                        }
//        //                    }
//        //                    else {
//        //                        $(".contenSlid:visible").slideUp(200);
//        //                        curObj.closest("h2").next(".contenSlid").slideDown(200);
//        //                    }
//        //                }
//        //                else {
//
//        //                    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
//
//        //                        var ieversion = new Number(RegExp.$1);
//        //                        if (ieversion < 8) {
//        //                            curObj.closest("h2").next(".contenSlid").show(0);
//        //                            $("#mcs_container").mCustomScrollbar("horizontal", 0, "easeOutCirc", 1, "fixed", "no", "no", 20);
//        //                        }
//        //                        else {
//        //                            curObj.closest("h2").next(".contenSlid").slideDown(200);
//        //                        }
//        //                    }
//        //                    else {
//        //                        curObj.closest("h2").next(".contenSlid").slideDown(200);
//        //                    }
//        //                }
//        //                $(this).text("סגור והמשך");
//        //                $(this).removeClass("upd");
//        //            }
//        //            else {
//        //                if (curObj.closest("h2").next(".contenSlid").is(":visible")) {
//        //                    curInd = $(".contenSlid").index($(".contenSlid:visible"));
//
//
//        //                    if ($(".contenSlid:eq(" + (curInd + 1) + ")").prev("H2").find(".vIcon").hasClass("upd")) {
//        //                        curInd++;
//        //                    }
//        //                    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
//        //                        var ieversion = new Number(RegExp.$1);
//        //                        if (ieversion < 8) {
//        //                            $(".contenSlid:visible").hide(0);
//        //                            $(".contenSlid:eq(" + (curInd + 1) + ")").show(0);
//        //                            $("#mcs_container").mCustomScrollbar("horizontal", 0, "easeOutCirc", 1, "fixed", "no", "no", 20);
//        //                        }
//        //                        else {
//        //                            $(".contenSlid:visible").slideUp(200);
//        //                            $(".contenSlid:eq(" + (curInd + 1) + ")").slideDown(200);
//        //                        }
//        //                    }
//        //                    else {
//        //                        $(".contenSlid:visible").slideUp(200);
//        //                        $(".contenSlid:eq(" + (curInd + 1) + ")").slideDown(200);
//        //                    }
//        //                }
//        //                $(this).text("עדכן");
//        //                $(this).addClass("upd");
//        //            }
//    });

   $( '.chooseBTN2').live("click", function () {
        var curObj = $(this);
        var elm = $(this).closest(".contenSlid").find(".phoneBTN");
        if ($(elm).attr('class') != undefined) {
            deviceFromChooseBtn = true;
        }
        $(this).closest(".contenSlid").find(".phoneBTN").click();
        $(this).parent('LI').addClass('selected');
        $(this).attr('class', 'phoneBTN');
        $(this).closest(".contenSlid").prev('h2').append('<span class="selectText">' + $(this).closest('LI').find(".detailsText").val() + '</span><a href="javascript:void(0);" title="" class="vIcon">סגור והמשך</a>');
        $(this).find(".btnLeft").text("המכשיר שלך");
        var clickTimer = setTimeout(function () {
            curObj.closest(".contenSlid").prev('h2').find(".vIcon").click();
        }, clickTimerSpeed);
    });

    $(".phoneBTN").live("click", function () {
        if (deviceFromChooseBtn) {
            $(this).parent('LI').removeClass('selected');
            $(this).attr('class', 'chooseBTN2 btn33');
            $(this).closest(".contenSlid").prev('h2').find(".selectText").remove();
            $(this).closest(".contenSlid").prev('h2').find(".vIcon").remove();
            $(this).find(".btnLeft").text("בחירה");
            deviceFromChooseBtn = false;
        }

    });
});

function goNext(th) {
    $(".contenSlid:visible").slideUp(200);
    $(th).closest(".contenSlid").next("h2").next(".contenSlid").slideDown(200);
}

function updateConnectionNShepment(elm, form) {
    var sessionEnd = "http://www.pelephone.co.il";
    var test = false;
    var window = 0;
    try {
        if (elm.attr('class') == 'nextBTN btn33')
            test = true;
    } catch (ex) {
    }
    try {
        if (elm.className == 'nextBTN btn33') {
            test = true;
            window = 1;
        }
    } catch (ex) {
    }
    if (test) {
        var url = '/SelfCare/PeleStoreConnectionNShepment.do?' + form;
        jQuery.ajax({
            type:"POST",
            url:url,
            success:function (data, textStatus, jqXHR) {
                var status = jqXHR.getResponseHeader('SESSION_STATUS');
                if (status == "ENDED") {
                    window.location = sessionEnd;
                    return;
                }
                $(elm).closest(".contenSlid").next("h2").next(".contenSlid").replaceWith(data);
                jQuery('#checkNewBanForm3').attr('autocomplete', 'off');
                $(elm).closest(".contenSlid").next("h2").remove();
                if ($('#eMail').val() != null && $('#eMail').val() != "") {
                    var mailVal = $('#eMail').val();
                    $('#email2').val(mailVal);
                }
                if (window == 1) {
                    $(elm).closest(".contenSlid").prev('h2').find(".vIcon").show();
//                    $($(elm).closest(".contenSlid").prev('h2').find(".vIcon")).attr('display','inline-block');
                }
//                $(elm).closest(".contenSlid").prev('h2').find(".vIcon").show();
                $(".contenSlid:visible").slideUp(200);
                $(elm).closest(".contenSlid").next("h2").next(".contenSlid").slideDown(200);
                jQuery(".vIcon").each(function () {
                    $(this).text("עדכן");
                    $(this).addClass("upd");
                });
                if (window) {
                    getShepmentData('non');
                }
            },
            complete:function (jqXHR, textStatus) {
            }
        });
    }
}

function googleAnalytics(deviceType, packageName, price, customerType, customerAction, stepName) {
//    _gaq.push(['_trackPageview', '/vps/sales/' + customerType + '/' + phaseNumber + '/' + sectionName + '/' + objName]);
//    'sales/Device Type/Package price/Customer Type/Customer Action/Step Name'
    if (customerType == '')
        customerType = 'null';

    if (packageName == '')
        packageName = 'null';

    if (price == '')
        price = 'null';

    if (stepName == '')
        stepName = 'null';

    if (deviceType == '')
        deviceType = 'No_Device';

    if (customerAction == '')
        customerAction = 'null';

    try {
        var data = 'sales/' + deviceType + "/" + packageName + '_' + price + '/' + customerType + '/' + customerAction + '/' + stepName;
        _gaq.push(['_trackPageview', data]);
    } catch (ex) {
    }
}
function googleAnalyticsTrackEvent(objectName, customerType, packageName, price, sectionName) {
//    _gaq.push(['_trackPageview', '/vps/sales/' + customerType + '/' + phaseNumber + '/' + sectionName + '/' + objName]);
    try {
        var data = packageName + '_' + price + '-' + customerType;
        _gaq.push(['_trackEvent', objectName, data, sectionName]);
        //alert(data);
    } catch (ex) {
    }
}

function updateFooterProg(elm, ind, phase) {
    var sessionEnd = "http://www.pelephone.co.il";
    if (elm.className == 'chooseBTN btn33') {
        var url = '/SelfCare/UpdateCampaign.do';
        var params = {selectedCampaignIndex:ind};
        jQuery.ajax({
            type:"POST",
            url:url,
            data:params,
            success:function (data, textStatus, jqXHR) {
                var status = jqXHR.getResponseHeader('SESSION_STATUS');
                if (status == "ENDED") {
                    window.location = sessionEnd;
                    return;
                }
            },
            complete:function (jqXHR, textStatus) {
                getFooterData(phase);
            }
        });
    }
}

function updateFooterDevice(elm, ind, phase) {
    var sessionEnd = "http://www.pelephone.co.il";
    if (elm.className == 'chooseBTN2 btn33') {
        var url = '/SelfCare/UpdateDevice.do';
        var params = {selectedDeviceIndex:ind};
        jQuery.ajax({ type:"POST",
            url:url,
            data:params,
            success:function (data, textStatus, jqXHR) {
//                var footerDeviceNameHid = $(".footerDeviceName" + ind).val();
//                var footerDevicePriceHid = $(".footerDevicePrice" + ind).val();
//                var footerDevicePaymentHid = $(".footerDevicePayment" + ind).val();
//                $("#footerDeviceName").html(footerDeviceNameHid);
//                $("#footerDevicePrice").html(footerDevicePriceHid);
//                $("#footerDevicePayment").html(footerDevicePaymentHid);
//                $('.selectedDeviceInd').val(ind);
                var status = jqXHR.getResponseHeader('SESSION_STATUS');
                if (status == "ENDED") {
                    window.location = sessionEnd;
                    return;
                }
            },
            complete:function (jqXHR, textStatus) {
                getFooterData(phase);
            }
        });
    }
}

function getFooterData(phase) {
   /* jQuery.ajax({ type:"POST",
        url:'/SelfCare/PeleStoreFooter.do',
        beforeSend:function () {
        },
        success:function (data) {
            jQuery('.reservationLine').html(data);
        },
        complete:function (jqXHR, textStatus) {
            
        }
    });*/
    footerBoxClick(phase);
}

function showNumbers(curInd) {
    if (curInd >= 1 || (curInd == 0 && !SHOWDEVICES)) {
        var url = '/SelfCare/PeleStoreNumbers.do';
        var returnData;
        jQuery.ajax({ type:"POST",
            url:url,
            beforeSend:function () {
                //jQuery('#numbersContent').hide();
                jQuery('#loadingImg').show();
            },
            success:function (data) {
                returnData = data;
                //jQuery('#numbersContent').show();
            },
            complete:function (jqXHR, textStatus) {
                var generalError = jqXHR.getResponseHeader('GENERAL_ERROR');
                if (generalError != null && generalError == 'TRUE') {
                    window.location = window.location.protocol + "//" + window.location.host + "/SelfCare/PeleStoreGeneralError.do";
                    return;
                }
                var sessionStatus = jqXHR.getResponseHeader('SESSION_STATUS');
                if (sessionStatus == 'ENDED') {
                    //window.location = "";
                }
                jQuery('#loadingImg').hide();

                jQuery('#numbersContent').html(returnData);
                jQuery('#PeleStoreSubscriberDetailsForm').attr('autocomplete', 'off');
                cleanInputNumber();
            }
        });
    }
}

function submitDataStage1(selectedNumberInd, sessionEnd,deviceType,packageName,price,customerType,ctnType) {
    if (selectedNumberInd >= 0) {
        var url = '/SelfCare/PeleStoreSelectedNumber.do';
        var params = {selectedNumberIndex:selectedNumberInd};
        var returnData;
        jQuery.ajax({ type:"POST",
            url:url,
            data:params,
            beforeSend:function () {
                //$('#numbersContent').hide();
                $('#loadingImg').show();
            },
            success:function (data) {
                returnData = data;
            },
            complete:function (jqXHR, textStatus) {
                var sessionStatus = jqXHR.getResponseHeader('SESSION_STATUS');
                var generalError = jqXHR.getResponseHeader('GENERAL_ERROR');
                var messageError = jqXHR.getResponseHeader('MSGERR');
                if (sessionStatus == 'ENDED') {
                    window.location = sessionEnd;
                    return;
                }
                if (generalError != null && generalError == 'TRUE') {
                    window.location = window.location.protocol + "//" + window.location.host + "/SelfCare/PeleStoreGeneralError.do";
                    return;
                }
                var URL = jqXHR.getResponseHeader('URL');
                var subscriberStatus = jqXHR.getResponseHeader('subscriberStatus');
                if (subscriberStatus != null && (subscriberStatus == 'A' || subscriberStatus == 'S')) {
                    $('#errorMessage').html($('#msgStatusA').html());
                }
                if (subscriberStatus != null && subscriberStatus == 'R') {
                    $('#errorMessage').html($('#msgStatusR').html());
                }
                if (URL != null && URL != '') {
                    googleAnalytics(deviceType,packageName,price,customerType,ctnType, 'Number_selection');
                    window.location = window.location.protocol + "//" + window.location.host + "/SelfCare" + URL;
                    return;
                } else {
                    if (returnData != null && returnData != '') {
                        $('#numbersContent').html(returnData);
                    }
                    jQuery('#PeleStoreSubscriberDetailsForm').attr('autocomplete', 'off');
                    $('#errorMessageChooseNumber').html(decodeURI(messageError).replace(/\+/g, ' '));
                    $('#loadingImg').hide();
                    //$('#numbersContent').show();
                    $('#errorMessage').show();
                }
            }
        });
    }
}

function cleanInputNumber() {
    //clean input
    $('.cleanInput').each(function () {
        var currentVal = this.value;
        $(this).focus(function () {
            if (this.value == currentVal) {
                this.value = '';
            }
        });
        $(this).blur(function () {
            if (this.value == '') {
                this.value = currentVal;
            }
        });
    });
}

function checkSubscriberNum() {
    var subsNum = $('#selectedCtnID').val();
    var phoneRegExp = /^05\d{8}$/;
    var errExist = false;
    $('#selCtnBusMsg').hide();
    $('#errorMessage').hide();
    $('#selCtnBusMsg').html('');
    if (subsNum == null || subsNum.length == 0) {
        $('#selectedCtnID').removeClass("textInp");
        $('#selectedCtnID').addClass("textInp_error");
        $('#selCtnBusMsg').html('חובה להזין שדה זה ');
        $('#selCtnBusMsg').show();
        errExist = true;
    } else if (subsNum.length < 10 || subsNum.search(phoneRegExp) == -1) {
        $('#selectedCtnID').removeClass("textInp");
        $('#selectedCtnID').addClass("textInp_error");
        $('#selCtnBusMsg').html('מספר המנוי שהוקלד אינו תקין');
        $('#selCtnBusMsg').show();
        errExist = true;
    } else {
        $('#selectedCtnID').removeClass("textInp_error");
        $('#selectedCtnID').addClass("textInp");
    }
    return !errExist;
}

function checkSmsCode() {
    if (!checkSubscriberNum()) {
        return false;
    }
    var smsNum = $('#smsCode').val();
    var smsRegExp = /^\d{5}|\d{6}$/;
    var errExist = false;
    $('#errorMessage').hide();
    $('#errorMessage').html('');
    if (smsNum == null || smsNum.length == 0) {
        $('#smsCode').removeClass("textInp");
        $('#smsCode').addClass("textInp_error");
        $('#errorMessage').html('חובה להזין שדה זה ');
        $('#errorMessage').show();
        errExist = true;
    } else if (smsNum.length < 5 || smsNum.search(smsRegExp) == -1) {
        $('#smsCode').removeClass("textInp");
        $('#smsCode').addClass("textInp_error");
        $('#errorMessage').html('הקוד שהזנת שגוי');
        $('#errorMessage').show();
        errExist = true;
    } else {
        $('#smsCode').removeClass("textInp_error");
        $('#smsCode').addClass("textInp");
    }
    return !errExist;
}

function submitFormAjax(operation, sessionEnd, code,deviceType,packageName,price,customerType,ctnType) {

//    googleAnalytics('ALL', '1', 'NumberSelection', code);

    $("[class=subSelected]").text($('#subSelected').value);
    $('#selectselectedCtn').text($('#selectsubSelected').value);

    var parameters = jQuery('#PeleStoreSubscriberDetailsForm').serialize();
    parameters = decodeURI(parameters);
    runAjax('/SelfCare/checkSubscriber.do' + '?' + parameters + '&act=' + operation, sessionEnd, deviceType,packageName,price,customerType,ctnType);
}

function runAjax(url, sessionEnd, deviceType,packageName,price,customerType,ctnType) {

    var returnData;
    jQuery.ajax({ type:"POST",
        url:url,
        context:document.body,
        beforeSend:function () {
            //$('#numbersContent').hide();
            $('#loadingImg').show();
        },
        success:function (data) {
            returnData = data;

        },
        complete:function (jqXHR, textStatus) {
            var responseHeader = jqXHR.getResponseHeader('EAI_STATUS');
            var Subscriber = jqXHR.getResponseHeader('SUB');
            var message = jqXHR.getResponseHeader('MESSAGE');
            var sessionStatus = jqXHR.getResponseHeader('SESSION_STATUS');
            var fatalError = jqXHR.getResponseHeader('FATALERR');
            var messageError = jqXHR.getResponseHeader('MSGERR');
            if (fatalError == 'true') {
                window.location = "/SelfCare/PeleStoreGeneralError.do";
                return;
            }
            if (sessionStatus == "ENDED") {
                window.location = sessionEnd;
                return;
                return;
            }
            var URL = jqXHR.getResponseHeader('URL');
            var subscriberStatus = jqXHR.getResponseHeader('subscriberStatus');
            if (message != null && message == 'UPG') {
                $('#errorMessage').html($('#msgStatusA').html());

            }
//            else if (subscriberStatus != null && subscriberStatus == 'R') {
//                $('#errorMessage').html($('#msgStatusR').html());
//            }
            $("#captchaImg").attr("src", $('#captchaSrc').val() + Math.floor(Math.random() * 100));
            if (URL != null && URL != '') {
//                if (subscriberStatus != null && subscriberStatus != '' && subscriberStatus != 'C') {
                    googleAnalytics(deviceType,packageName,price,customerType,ctnType, 'Number_selection');
//                } else {
//                    googleAnalytics(deviceType,packageName,price,customerType,ctnType, 'Number_selection');
//                }
                window.location = window.location.protocol + "//" + window.location.host + "/SelfCare" + URL;
                return;
            } else {
                $('#numbersContent').html(returnData);
                jQuery('#PeleStoreSubscriberDetailsForm').attr('autocomplete', 'off');
                if (messageError != null && messageError != '') {
                    $('#errorMessage').html(decodeURI(messageError).replace(/\+/g, ' '));
                }
                $('#loadingImg').hide();
                //$('#numbersContent').show();
                $('#errorMessage').show();
            }
        }});
}

function changeCaptcha() {
    $("#captchaImg").attr("src", $('#captchaSrc').val() + Math.floor(Math.random() * 100));
}

function loadPaymentPage(th, sessionEnd) {
    var url = '/SelfCare/PeleStoreStagePaymentInit.do';
    jQuery.ajax({ type:"POST",
        url:url,
        success:function (data) {
            goNext(th);
            $('#paymentContent').html(data);
            $('#paymentContent').show();
        },
        complete:function (jqXHR, textStatus) {
            var sessionStatus = jqXHR.getResponseHeader('SESSION_STATUS');
            if (sessionStatus == 'ENDED') {
                window.location = sessionEnd;
            }
        }
    });
}

function getData4(th, next, sessionEnd, deviceType,packageName,price,customerType,ctnType) {
    googleAnalytics(deviceType,packageName,price,customerType,ctnType, 'Address_Details');
    var pubDetails = true;
    var marketingInformation = true;
    if (jQuery('#pubDetailsN').attr('class') == 'btn25G_select') {
        pubDetails = false;
    }
    if (jQuery('#marketingInformationN').attr('class') == 'btn25G_select') {
        marketingInformation = false;
    }
    var parameters = jQuery('#checkNewBanForm3').serialize();
    parameters = decodeURI(parameters);
    runAjax4('/SelfCare/PeleStoreConnectionNShepment.do?action=save&pubDetails=' + pubDetails + '&marketingInformation=' + marketingInformation + "&sendBillInMail=" + jQuery('#mailcheck').attr('checked')
        + '&' + parameters, th, next, sessionEnd);
//        runAjax2('/SelfCare/checkNewBan.do' + '?' + jQuery('#checkNewBanForm').serialize());
}

function runAjax4(url, th, next, sessionEnd) {
    jQuery.ajax({ type:"POST",
        url:url,
        context:document.body,
        beforeSend:function () {
            jQuery('#errMsg').hide();
            jQuery('#loadingImg4').show();
        },
        success:function (data) {
        },
        complete:function (jqXHR, textStatus) {
            var status = jqXHR.getResponseHeader('SESSION_STATUS');
            if (status == "ENDED") {
                window.location = sessionEnd;
            }
            var responseHeader = jqXHR.getResponseHeader('EAI_STATUS');
            var errMsgCode = jqXHR.getResponseHeader('errMsg');
            if (jQuery(th).closest(".contenSlid").prev('h2').find(".vIcon").is(":visible")) {
                var icon = jQuery(th).closest(".contenSlid").prev('h2').find(".vIcon");
                icon.text("עדכן");
                icon.addClass("upd");
            } else {
                jQuery(th).closest(".contenSlid").prev('h2').find(".vIcon").show();
            }
            if (next == 'P')
                loadPaymentPage(th);
            else if (next == 'N') {
                window.location = window.location.protocol + "//" + window.location.host + "/SelfCare/PeleStore-Deal-Confirmation.do";
            }
            jQuery('#loadingImg4').hide();
        }});
}

function checkValues2() {
    var retValue = true;
    var eMail = jQuery('#eMail').val();
    var eMailValid = jQuery('#email2').val();
    var accPassword = jQuery('#accPassword').val();
    var subFName = jQuery('#subFName').val();
    var subLName = jQuery('#subLName').val();
    var phone = jQuery('#phone').val();
    var simtype = jQuery('.simType:checked').val();
    var street = jQuery('#streetStyle').val();
    var houseNo = jQuery('#houseNo').val();
//    var appNo = jQuery('#appNo').val();
    var city = jQuery('#cityCode').val();
    var simNumber = jQuery("#simNumber").val();

//    removeErrorBox(jQuery('#eMail'));
//    removeErrorBox(jQuery('#email2'));
//    removeErrorBox(jQuery('#accPassword'));
//    removeErrorBox(jQuery('#subFName'));
//    removeErrorBox(jQuery('#subLName'));
//    removeErrorBox(jQuery('#simType'));
//    removeErrorBox(jQuery('#streetStyle'));
//    removeErrorBox(jQuery('#houseNo'));
//    removeErrorBox(jQuery('#cityCode'));
    if (jQuery('#simNumber').length > 0) {
        removeErrorBox(jQuery('#simNumber'));
    }

    jQuery("#emailMsg").hide();
    jQuery("#emailValidMsg").hide();
    jQuery("#accPasswordMsg").hide();
    jQuery("#subFNameMsg").hide();
    jQuery("#subLNameMsg").hide();
    jQuery("#phoneMsg").hide();
    jQuery("#simTypeMsg").hide();
    jQuery("#streetStyleMsg").hide();
    jQuery("#houseNoMsg").hide();
//    jQuery("#appNoMsg").hide();
    jQuery("#cityMsg").hide();
    jQuery("#pubDetailsMsg").hide();
    jQuery("#marketingInformationMsg").hide();
    jQuery("#simNumberMsg").hide();


    var errElm = null;


    if (eMail.length == 0) {
        jQuery("#emailMsg").html("חובה להזין שדה זה");
        jQuery("#emailMsg").show();
        retValue = false;
        setErrorBox(jQuery('#eMail'));
        if (errElm == null) {
            errElm = jQuery("#eMail");
        }
    } else if (emailValidate(eMail)) {
        jQuery("#emailMsg").html("כתובת הדואר האלקטרוני אינה תקינה ");
        jQuery("#emailMsg").show();
        retValue = false;
        setErrorBox(jQuery('#eMail'));
        if (errElm == null) {
            errElm = jQuery("#eMail");
        }
    } else if (eMail != eMailValid) {
        jQuery("#emailValidMsg").html("אין התאמה בין כתובת דואר אלקטרוני לאימות דואר אלקטרוני");
        jQuery("#emailValidMsg").show();
        retValue = false;
        setErrorBox(jQuery('#email2'));
        if (errElm == null) {
            errElm = jQuery("#email2");
        }
    }

    if (phoneValidate(phone)) {
        retValue = false;
        setErrorBox(jQuery('#phone'));
        if (errElm == null) {
            errElm = jQuery("#phone");
        }
    }

    if (jQuery('#accPassword').length > 0 && accPasswordValidate(accPassword)) {
        retValue = false;
        setErrorBox(jQuery('#accPassword'));
        if (errElm == null) {
            errElm = jQuery("#accPassword");
        }
    }

    if (subFName.length < 2) {
        jQuery("#subFNameMsg").html("יש להזין שם המכיל לפחות שני תווים");
        jQuery("#subFNameMsg").show();
        retValue = false;
        setErrorBox(jQuery('#subFName'));
        if (errElm == null) {
            errElm = jQuery("#subFName");
        }
    } else if (nameValidate(subFName, jQuery("#subFNameMsg"), jQuery('#subFName'))) {
        retValue = false;
        if (errElm == null) {
            errElm = jQuery("#subFName");
        }
    }

    if (subLName.length < 2) {
        jQuery("#subLNameMsg").html("יש להזין שם המכיל לפחות שני תווים");
        jQuery("#subLNameMsg").show();
        retValue = false;
        setErrorBox(jQuery('#subLName'));
        if (errElm == null) {
            errElm = jQuery("#subLName");
        }
    } else if (nameValidate(subLName, jQuery("#subLNameMsg"), jQuery('#subLName'))) {
        retValue = false;
        if (errElm == null) {
            errElm = jQuery("#subLName");
        }
    }

    if (jQuery('#pubDetailsN').attr('class') != 'btn25G_select' &&
        jQuery('#pubDetailsY').attr('class') != 'btn25_select') {
        jQuery("#pubDetailsMsg").html("חובה לבחור אחת מהאפשרויות");
        jQuery("#pubDetailsMsg").show();
        if (errElm == null) {
            errElm = jQuery("#pubDetailsMsg");
        }
        retValue = false;
    }
    if (jQuery('#marketingInformationN').attr('class') != 'btn25G_select' &&
        jQuery('#marketingInformationY').attr('class') != 'btn25_select') {
        jQuery("#marketingInformationMsg").html("חובה לבחור אחת מהאפשרויות");
        jQuery("#marketingInformationMsg").show();
        if (errElm == null) {
            errElm = jQuery("#marketingInformationMsg");
        }
        retValue = false;
    }

    if (jQuery('#cityCode').length > 0 &&
        (city.length == 0 || city == '0')) {
        jQuery("#cityMsg").html("חובה להזין שדה זה");
        jQuery("#cityMsg").show();
        setErrorBox(jQuery('#cityCode').next('.ui-combobox').find('.ui-autocomplete-input'));
        if (errElm == null) {
            errElm = jQuery("#cityCode");
        }
        retValue = false;
    }

    if (jQuery('#streetStyle').length > 0 && street.length == 0) {
        jQuery("#streetStyleMsg").html("חובה להזין שדה זה");
        jQuery("#streetStyleMsg").show();
        setErrorBox(jQuery('#streetStyle'));
        retValue = false;
    } else if (jQuery('#streetStyle').length == 0) {
        jQuery("#streetStyleMsg").html("חובה להזין שדה זה");
        jQuery("#streetStyleMsg").show();
        retValue = false;
    }
    else if (nameValidate(street, jQuery("#streetStyleMsg"), jQuery('#streetStyle'))) {
        retValue = false;
    }

//    if (jQuery('#street').length > 0 && jQuery('#street').attr("selectedIndex") == 0) {
//        jQuery("#streetStyleMsg").html("חובה להזין שדה זה");
//        jQuery("#streetStyleMsg").show();
//        setErrorBox(jQuery('#street').next('.ui-combobox').find('.ui-combobox-input'));
//        retValue = false;
//    }

    if (jQuery('#houseNo').length > 0 && houseNo.length == 0) {
        jQuery("#houseNoMsg").html("חובה להזין שדה זה");
        jQuery("#houseNoMsg").show();
        setErrorBox(jQuery('#houseNo'));
        retValue = false;
    } else if (jQuery('#houseNo').length == 0) {
        jQuery("#houseNoMsg").html("חובה להזין שדה זה");
        jQuery("#houseNoMsg").show();
        retValue = false;
    }
//    if (jQuery('#appNo').length > 0 && appNo.length == 0) {
//        jQuery("#appNoMsg").html("חובה להזין שדה זה");
//        jQuery("#appNoMsg").show();
//        retValue = false;
//    }

    if (jQuery('.simType').length > 0 && simtype != 'sim' && simtype != 'minisim' && simtype != 'nanosim') {
        jQuery("#simTypeMsg").html("חובה לבצע בחירה");
        jQuery("#simTypeMsg").show();
        retValue = false;
    }

    if (jQuery("#simNumber").length > 0) {
        if (simNumber.length < 19) {
            jQuery("#simNumberMsg").html("יש להזין מספר סים המכיל לפחות 19 ספרות");
            jQuery("#simNumberMsg").show();
            retValue = false;
            setErrorBox(jQuery("#simNumber"));
        } else {
            var stt = checkSimValidation();
            if (stt == '1') {
                jQuery("#simNumberMsg").html("מספר הסים שהוזן אינו תקין");
                jQuery("#simNumberMsg").show();
                retValue = false;
                setErrorBox(jQuery("#simNumber"));
            } else if (stt == '2') {
                jQuery("#simNumberMsg").html("ארעה שגיאה, אנא נסה שנית");
                jQuery("#simNumberMsg").show();
                retValue = false;
                setErrorBox(jQuery("#simNumber"));
            }
        }
    }

    if (errElm != null) {
        jQuery(window).scrollTop(errElm.position().top);
    }

    return retValue;
}

function subTrnsvalidation(nxtBtn, deviceType,packageName,price,customerType,ctnType) {
    googleAnalytics(deviceType,packageName,price,customerType,ctnType, 'Client_Details');
    var sessionEnd = "http://www.pelephone.co.il";
    var parameters = jQuery('#customerDetailsForm').serialize();
    parameters = decodeURI(parameters);
    url = '/SelfCare/peleStoreStageTrnV.do?' + parameters;
    jQuery.ajax({ type:"POST",
        url:url,
//        context:document.body,
        beforeSend:function () {
            jQuery('#cardNum').attr('class', 'input162');
            jQuery('#accPas').attr('class', 'input162');
            jQuery('#cardError').html('');
            jQuery('#codeError').html('');

        },
        success:function (data, textStatus, jqXHR) {
            var sessionStatus = jqXHR.getResponseHeader('SESSION_STATUS');
            var status = jqXHR.getResponseHeader('STATUS');
            if (sessionStatus == "ENDED") {
                window.location = sessionEnd;
            }
            if (status != null && status == 'VALID') {
                jQuery('#validationVIcon').show();
                goNext(nxtBtn);
                jQuery('#customerInfo').html(data);
                jQuery('#customerInfo').show();
                jQuery('#customerInfo').find('.contenSlid').show();
                jQuery(".vIcon").each(function () {
                    $(this).text("עדכן");
                    $(this).addClass("upd");
                });
            } else {
                jQuery('#validationSlide').replaceWith(data);
                jQuery(".vIcon").each(function () {
                    if ($(this).hasClass('upd')) {
                        $(this).text("סגור והמשך");
                        $(this).removeClass('upd')
                    }
                });
            }
        },
        complete:function (jqXHR, textStatus, data) {
        }});
}

function subTrns(deviceType,packageName,price,customerType,ctnType) {
    googleAnalytics(deviceType,packageName,price,customerType,ctnType, 'Address_Details');
    var sessionEnd = "http://www.pelephone.co.il";
    var parameters = jQuery('#peleStoreStg2CustInfoForm').serialize();
    parameters = decodeURI(parameters);
    url = '/SelfCare/peleStoreCustomerInfov.do?' + parameters + "&sendBillInMail=" + $('#mailcheck').attr('checked');
    jQuery.ajax({ type:"POST",
        url:url,
//        context:document.body,
        beforeSend:function () {
        },
        success:function (data, textStatus, jqXHR) {
            var sessionStatus = jqXHR.getResponseHeader('SESSION_STATUS');
            var status = jqXHR.getResponseHeader('STATUS');
            var url = jqXHR.getResponseHeader('URL');
            if (sessionStatus == "ENDED") {
                window.location = sessionEnd;
            }
            if (status == 'VALID') {
                window.location = url;
            } else {

                jQuery('#customerInfo').html(data);
            }
        },
        complete:function (jqXHR, textStatus, data) {
        }});
}

function nameValidate(name, msgbox, control) {
    var reg = /^[a-zA-Z0-9א-ת '"-]*$/;
    if (!reg.test(name)) {
        msgbox.html("השם מכיל תווים אסורים");
        msgbox.show();
        setErrorBox(control);
        return true;
    }
    return false;
}

function emailValidate(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email)) {
        return false;
    }
    return true;
}

function accPasswordValidate(accnum) {
    var acc = /^[1-9][0-9]*$/;
    var errExist = false;
    jQuery('#accPasswordMsg').hide();
    jQuery('#accPasswordMsg').html('');
    if (accnum == null || accnum.length < 4) {
        jQuery('#accPasswordMsg').html('קוד הזיהוי האישי צריך לכלול בדיוק ארבע ספרות.');
        jQuery('#accPasswordMsg').show();
        errExist = true;
    } else if (accnum.search(acc) == -1) {
        jQuery('#accPasswordMsg').html('קוד הזיהוי האישי שהוקלד אינו תקין');
        jQuery('#accPasswordMsg').show();
        errExist = true;
    }
    return errExist;
}

function phoneValidate(subsNum) {
//    var phoneRegExp = /^0\[0-9]+$/;
    var phoneRegExp = /^[0][0-9]*$/;
    var errExist = false;
    jQuery('#phoneMsg').hide();
    jQuery('#phoneMsg').html('');
    if (subsNum == null || subsNum.length == 0) {
        jQuery('#phoneMsg').html('חובה להזין שדה זה');
        jQuery('#phoneMsg').show();
        errExist = true;
    } else if (subsNum.length < 9 || subsNum.search(phoneRegExp) == -1) {
        jQuery('#phoneMsg').html('מספר הטלפון שהוקלד אינו תקין');
        jQuery('#phoneMsg').show();
        errExist = true;
    }
    return errExist;
}

function docReadyStage3(location, sessionEnd, phase) {
    jQuery('.btn25G').click(function () {
        jQuery(this).removeClass('btn25G');
        jQuery(this).addClass('btn25G_select');
        var array = jQuery(this).attr('id').split('_');
        jQuery('#checkbox' + array[1]).attr('checked', false);

        jQuery(this).prev().removeClass('btn25_select');
        jQuery(this).prev().addClass('btn25');
    });

    jQuery('.btn25').click(function () {
        jQuery(this).removeClass('btn25');
        jQuery(this).addClass('btn25_select');
        var array = jQuery(this).attr('id').split('_');
        jQuery('#checkbox' + array[1]).attr('checked', true);

        jQuery(this).next().removeClass('btn25G_select');
        jQuery(this).next().addClass('btn25G');
    });
    jQuery('.approvBTN').click(function () {
        submitStage3ServicesForm(this, location, sessionEnd);
    });


    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    if ($.browser.msie && parseInt($.browser.version, 10) == 8) {
        $(".tooltip_triger").tooltip({
            position:['top', 'left'],
            // position fine tuning
            offset:[1, 115]
        });
    } else if (is_chrome) {
        $(".tooltip_triger").tooltip({
            position:['top', 'left'],
            // position fine tuning
            offset:[1, 115]
        });
    } else {
        $(".tooltip_triger").tooltip({
            position:['top', 'left'],
            // position fine tuning
            offset:[1, 115]
        });
    }

    getFooterData(phase);
}

function submitStage3ServicesForm(btn, location, sessionEnd) {
    if (checkButtonsSelected()) {
//        googleAnalytics('ALL', '3', 'Services', 'End');
        var url = jQuery('#servicesForm').attr('action') + '?' + jQuery('#servicesForm').serialize();

        jQuery.ajax({ type:"POST",
            url:url,
            context:document.body,
            beforeSend:function () {
                jQuery('#errorService').hide();

                if (jQuery(btn).hasClass('btn33')) {
                    jQuery(btn).removeClass('btn33');
                    jQuery(btn).addClass('btn33_disable');
                    jQuery(btn).attr('onclick', '').unbind('click');
                    jQuery(btn).closest('DIV.boxMidL').find('DIV.opacitydiv').attr('class', 'opacitydiv opacity ');
                }
                jQuery('.loader').show();
            },
            success:function (data, textStatus, jqXHR) {
                var genError = "/SelfCare/PeleStoreGeneralError.do";

                var status = jqXHR.getResponseHeader('SESSION_STATUS');
                if (status == "ENDED") {
                    window.location = sessionEnd;
                }

                var errMsg = jqXHR.getResponseHeader('errMsg');
                if (errMsg == "ServiceError") {
                    window.location = genError;
                }
                else {
                    jQuery('.checkDiv').removeClass('checkDiv_disable');
                    jQuery('#mobLinkDis').hide();
                    jQuery('#mobLinkEn').show();
                    jQuery('#contractLinkDis').hide();
                    jQuery('#contractLinkEn').show();
                    jQuery('#generalLinkDis').hide();
                    jQuery('#generalLinkEn').show();
                    jQuery('#commentLinkEn').show();
                    jQuery('#commentLinkDis').hide();
                    jQuery('#checkbox_mobility').removeAttr("disabled");
                    jQuery('#checkbox_contract').removeAttr("disabled");
                    jQuery('#checkbox_general').removeAttr("disabled");
                    jQuery('#checkbox_underAge').removeAttr("disabled");
                    jQuery('.checkDiv INPUT').attr("disabled", false);
                    jQuery('.marginT').click(function () {
                        submitStage3Deal(location, sessionEnd);
                    });
                    jQuery('.btn25_select').attr('onclick', '').unbind('click');
                    jQuery('.btn25G_select').attr('onclick', '').unbind('click');
                    jQuery('.btn25').attr('onclick', '').unbind('click');
                    jQuery('.btn25G').attr('onclick', '').unbind('click');
                    //jQuery('.btn25').removeClass('btn25').addClass('btn25_disabled ');
                    //jQuery('.btn25G').removeClass('btn25G').addClass('btn25G_disabled ');

                    jQuery('.marginT').attr('class', 'btn33 L marginT');
                }
            },
            complete:function () {
                jQuery('.loader').hide();
            }
        });
    }
}

function checkButtonsSelected() {

    jQuery('.error').each(function () {
        jQuery(this).removeClass('error');
        jQuery(this).addClass('error_hide');
    });

    var valid = true;
    jQuery('.btn25G').each(function () {
        if (jQuery(this).prev().hasClass('btn25')) {
            valid = false;
            var array = jQuery(this).attr('id').split('_');
            jQuery('#error_' + array[1]).removeClass('error_hide');
            jQuery('#error_' + array[1]).addClass('error');
        }
    });

    return valid;
}

function runStage3Pdf(url, sessionEnd, customerType, packageName, price) {

    jQuery.ajax({ type:"POST",

        url:url,
        context:document.body,
        beforeSend:function () {
        },
        success:function (data, textStatus, jqXHR) {
            var status = jqXHR.getResponseHeader('SESSION_STATUS');
            if (status == "ENDED") {
                window.location = sessionEnd;
            }
            else {
                window.open(url);
            }
        }
    });
    googleAnalyticsTrackEvent('Content Services-Terms', customerType, packageName, price, packageName);
}

function submitStage3Deal(sessionEnd) {
    var ctnType = jQuery('#ctnType').val();
    var packageName = jQuery('#packageName').val();
    var price = jQuery('#price').val();
    var deviceType = jQuery('#deviceType').val();
    var customerType = jQuery('#customerType').val();

    if (checkBoxesMarked()) {
        jQuery('.loader').show();
        googleAnalytics(deviceType,packageName,price,customerType,ctnType, 'End');
        var url = jQuery('#completeDealForm').submit();
    }
}

function checkBoxesMarked() {
    var valid = true;
    if (!checkBoxMarked("checkbox_mobility", "mobilityError")) {
        valid = false;
    }
    if (!checkBoxMarked("checkbox_contract", "contractError")) {
        valid = false;
    }
    if (!checkBoxMarked("checkbox_contract1", "contractError1")) {
        valid = false;
    }
    if (!checkBoxMarked("checkbox_general", "generalError")) {
        valid = false;
    }
    if (!checkBoxMarked("checkbox_underAge", "underAgeError")) {
        valid = false;
    }
    return valid;
}

function checkBoxMarked(checkboxId, errorId) {
    if (!jQuery('#' + checkboxId).attr('checked') && jQuery('#' + checkboxId).length != 0) {
        jQuery('#' + errorId).show();
        return false;
    }
    else {
        jQuery('#' + errorId).hide();
        return true;
    }

}

function showExtraInfo(url, popupTitle, popupWidth, popupHeight) {
    $('#PopupMoreInfo').show();
    $('#popupContent').hide();
    if (url != '') {
        $('#PopupMoreInfo')[0].src = '';
        $('#PopupMoreInfo')[0].src = url;
    } else {
        $('#popupContent').html('<iframe id="PopupMoreInfo" width="100%" height="100%" style="border: none; background: white"></iframe>');
    }

    $('#PopupTitle').html(popupTitle);
    if (popupWidth == null || popupWidth == 'undefined') {
        $('#ExtraPopup').width('1010px');
    } else {
        $('#ExtraPopup').width(popupWidth + 'px');
    }

    if (popupHeight == null || popupHeight == 'undefined') {
        $('#ExtraPopup').height('700px');
    } else {
        $('#ExtraPopup').height(popupHeight + 'px');
    }
    $("#ExtraPopup").bPopup({ modalColor:'#FFF', follow:[false, false] });
    return false;
}

function showExtraInfo2(popupTitle, content, popupWidth, popupHeight) {
    if (content != "" && content != null) {
        $('#popupContent').html(content);
        $('#popupContent').css('font', '1.5em/19px arial');
        $('#popupContent').find("P").css('margin-right', '16px');
        $('#popupContent').find("P").css('margin-left', '16px');
        $('#PopupMoreInfo').hide();
        $('#popupContent').show();
        $('#popupBottom').css('width', '399px');

        $('#PopupTitle').html(popupTitle);
        if (popupWidth == null || popupWidth == 'undefined') {
            $('#ExtraPopup').width('400px');
        } else {
            $('#ExtraPopup').width(popupWidth + 'px');
        }

        if (popupHeight == null || popupHeight == 'undefined') {
            $('#ExtraPopup').height('200px');
        } else {
            $('#ExtraPopup').height(popupHeight + 'px');
        }
        $("#ExtraPopup").bPopup({ modalColor:'#FFF', follow:[false, false] });
    }
    return false;
}
function setErrorBox(elem) {
    if (jQuery(elem).attr('class').indexOf('_error') < 0 && jQuery(elem).attr('class').indexOf('select') < 0) {
        jQuery(elem).attr('class', jQuery(elem).attr('class').replace('input82', 'input82_error'));
        jQuery(elem).attr('class', jQuery(elem).attr('class').replace('input252', 'input252_error'));
        jQuery(elem).attr('class', jQuery(elem).attr('class').replace('ui-combobox-input', 'ui-combobox-input_error'));
        if (jQuery(elem).attr('class').indexOf('input82') < 0 && jQuery(elem).attr('class').indexOf('input252'))
            jQuery(elem).attr('class', jQuery(elem).attr('class') + '_error');
//        if (jQuery(elem).attr('class').indexOf('ui-combobox-input') < 0)
//            jQuery(elem).attr('class', jQuery(elem).attr('class') + '_error');
        jQuery(elem).keydown(function (event) {
            removeErrorBox(elem);
        });
    } else if (jQuery(elem).attr('class').indexOf('select') >= 0 && jQuery(elem).attr('class').indexOf('_error') < 0) {
        jQuery(elem).attr('class', jQuery(elem).attr('class').replace('select252', 'select252_error'));
        jQuery(elem).attr('class', jQuery(elem).attr('class').replace('select119', 'select252_error'));
        jQuery(elem).mousedown(function (event) {
            removeErrorBox(elem);
        });
    }
}

function removeErrorBox(elem) {
    jQuery(elem).attr('class', jQuery(elem).attr('class').replace('_error', ''));
}

function checkEnterPress(e) {
    var key;
    if (window.event) {
        key = window.event.keyCode;     //IE
    } else {
        key = e.which;     //firefox
    }
    if (key == 13) {
        return false;
    }
    return true;
}

function linkLog(linkLogType) {
    var url = '/SelfCare/peleStorelinkLog.do';
    var params = {logType:linkLogType};
    jQuery.ajax({ type:"POST",
        url:url,
        data:params,
        success:function (data) {
        }
    });
}

function doubleClick_ConnectionNShipment() {
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    var myScript = document.createElement("img");
    myScript.src = "https://ad-emea.doubleclick.net/activity;src=3718415;type=digit802;cat=inser279;ord=1;num='" + a + "'?";
    myScript.width = "1";
    myScript.height = "1";
    myScript.alt = "";
    document.body.appendChild(myScript);
}

function footerBoxClick(phase) {
alert("click");
    $('.footerBox').toggle(function () {
        _gaq.push(['_trackEvent', 'MyCart', 'Click', phase]);
        $('.footerBox').animate({ "top":"-50px", "height":"130px" }, 200);
        $('.footerBox').addClass("open");
    }, function () {
        $('.footerBox').animate({ "top":"19px", "height":"46px" }, 200);
        $('.footerBox').removeClass("open");
    });
}

function getServicesList() {
    var action = '/SelfCare/valueAddedServices.do';
    $.ajax({
        url:action,
        success:function (data, textStatus, jqXHR) {
            $('#bluecontent').html(data);
            var Status = jqXHR.getResponseHeader('STATUS');
            if (Status != null && Status == 'ERROR') {
                $('#addservicelink').hide();
            }
        },
        complete:function (jqXHR, textStatus) {
        }, error:function () {
            $('#addservicelink').hide();
        }
    });
}

function createSAForm(index, sendmail, ctnType, packageName, price, socName) {
    var action = '/SelfCare/peleStoreAddSocForm.do';
    var parameters = $('#valueAddedServicesForm').serialize() + "&INDEX=" + index + "&SENDMAIL=" + sendmail;
    jQuery('#loadingImg').show();
    parameters = decodeURI(parameters);
    $.ajax({
        url:action,
        data:parameters,
        context:document.body,
        success:function (data, textStatus, jqXHR) {
            if (sendmail == 'false') {
                var formStatus = jqXHR.getResponseHeader('FORM_STATUS');
                if (formStatus == "ERROR") {
                    jQuery('#ERROR' + index).show();
                    jQuery('#AERROR' + index).hide();
                    jQuery('#price' + index).hide();
                    jQuery('#check' + index).hide();
                    jQuery('#btn33' + index).hide();
                }
                window.open(action + "?INDEX=" + index);
                jQuery('#loadingImg').hide();
            }
        }
    });
    googleAnalyticsTrackEvent('Content Services-About', ctnType, packageName, price, socName);
}

function changeBtn33Status(id, index) {
    if ($('#' + id).is(':checked')) {
        $('#btn33' + index).addClass("btn33");
        $('#btn33' + index).removeClass("btn33_disable");
    } else {
        $('#btn33' + index).addClass("btn33_disable");
        $('#btn33' + index).removeClass("btn33");
    }
}

function addSoc(pointer, index,deviceType,packageName,price,customerType,ctnType, socName) {
    if (jQuery(pointer).attr('class') == 'btn33') {
        var action = '/SelfCare/pelestoreaddsoc.do';
        var parameters = $('#valueAddedServicesForm').serialize() + "&INDEX=" + index;
        parameters = decodeURI(parameters);
        jQuery('#loadingImg').show();
        $.ajax({
            url:action,
            data:parameters,
            success:function (data, textStatus, jqXHR) {
            },
            complete:function (jqXHR, textStatus) {
                var addStatus = jqXHR.getResponseHeader('ADDSOC_STATUS');
                jQuery('#price' + index).hide();
                jQuery('#check' + index).hide();
                jQuery('#btn33' + index).hide();
                if (addStatus == "SUCCESS") {
                    googleAnalytics(deviceType,packageName,price,customerType,ctnType, socName);
                    jQuery('#SUCCESS' + index).show();
                    jQuery('#PSUCCESS' + index).show();
                    createSAForm(index, "true");
                } else {
                    jQuery('#ERROR' + index).show();
                    jQuery('#AERROR' + index).hide();
                }
                jQuery('#loadingImg').hide();
            }
        });
    } else {

    }
}

function goToByScroll(id) {
    // Remove "link" from the ID
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
            scrollTop:$("#" + id).offset().top},
        'slow');
}

function checkSimValidation() {
    var url = '/SelfCare/PeleStoreCheckSimValidation.do';//+ jQuery('#PeleStoreDealerPageForm').serialize();
    var params = jQuery('#checkNewBanForm3').serialize();
    var retval = '1';
    params['disable_heb_conv'] = 'true';
    jQuery.ajax({
        type:"POST",
        url:url,
        data:params,
        async:false,
        success:function (data, textStatus, jqXHR) {
        },
        complete:function (jqXHR, textStatus) {
            var simStatus = jqXHR.getResponseHeader('SIM_STATUS');
            if (simStatus != null) {
                retval = simStatus;
            }
        }
    });
    return retval;
}

function extDeviceCheckShipmentData() {
    var retValue = true;
    var eMail = jQuery('#eMail').val();
    var subFName = jQuery('#subFName').val();
    var subLName = jQuery('#subLName').val();
    var eMailValid = jQuery('#email2').val();
    var phone = jQuery('#phone').val();
    var street = jQuery('#streetStyle').val();
    var houseNo = jQuery('#houseNo').val();
    var city = jQuery('#cityCode').val();
    var id = jQuery('#id').val();


    jQuery("#emailMsg").hide();
    jQuery("#emailValidMsg").hide();
    jQuery("#phoneMsg").hide();
    jQuery("#streetStyleMsg").hide();
    jQuery("#houseNoMsg").hide();
    jQuery("#cityMsg").hide();
    jQuery("#subFNameMsg").hide();
    jQuery("#subLNameMsg").hide();
    jQuery("#personalIdMsg").hide();

    var errElm = null;
    if (eMail.length == 0) {
        jQuery("#emailMsg").html("חובה להזין שדה זה");
        jQuery("#emailMsg").show();
        retValue = false;
        setErrorBox(jQuery('#eMail'));
        if (errElm == null) {
            errElm = jQuery("#eMail");
        }
    } else if (emailValidate(eMail)) {
        jQuery("#emailMsg").html("כתובת הדואר האלקטרוני אינה תקינה ");
        jQuery("#emailMsg").show();
        retValue = false;
        setErrorBox(jQuery('#eMail'));
        if (errElm == null) {
            errElm = jQuery("#eMail");
        }
    } else if (eMail != eMailValid) {
        jQuery("#emailValidMsg").html("אין התאמה בין כתובת דואר אלקטרוני לאימות דואר אלקטרוני");
        jQuery("#emailValidMsg").show();
        retValue = false;
        setErrorBox(jQuery('#email2'));
        if (errElm == null) {
            errElm = jQuery("#email2");
        }
    }

    if (phoneValidate(phone)) {
        retValue = false;
        setErrorBox(jQuery('#phone'));
        if (errElm == null) {
            errElm = jQuery("#phone");
        }
    }

    validateName(subFName, 'subFNameMsg', 'subFName', errElm, retValue);
    validateName(subLName, 'subLNameMsg', 'subLName', errElm, retValue);

    if (jQuery('#personalId').length > 0 && !validateID('personalId', 'personalIdMsg')) {
        jQuery("#personalIdMsg").show();
        setErrorBox(jQuery('#personalId'));
        if (errElm == null) {
            errElm = jQuery('#personalId');
        }
        retValue = false;
    }

    if (jQuery('#cityCode').length > 0 &&
        (city.length == 0 || city == '0')) {
        jQuery("#cityMsg").html("חובה להזין שדה זה");
        jQuery("#cityMsg").show();
        setErrorBox(jQuery('#cityCode').next('.ui-combobox').find('.ui-autocomplete-input'));
        if (errElm == null) {
            errElm = jQuery("#cityCode");
        }
        retValue = false;
    } else if (jQuery('#cityCode').length == 0) {
        jQuery("#cityMsg").html("חובה להזין שדה זה");
        jQuery("#cityMsg").show();
        retValue = false;
    }

    if (jQuery('#streetStyle').length > 0 && street.length == 0) {
        jQuery("#streetStyleMsg").html("חובה להזין שדה זה");
        jQuery("#streetStyleMsg").show();
        setErrorBox(jQuery('#streetStyle'));
        retValue = false;
    } else if (jQuery('#streetStyle').length == 0) {
        jQuery("#streetStyleMsg").html("חובה להזין שדה זה");
        jQuery("#streetStyleMsg").show();
        retValue = false;
    }
    else if (nameValidate(street, jQuery("#streetStyleMsg"), jQuery('#streetStyle'))) {
        retValue = false;
    }

    if (jQuery('#houseNo').length > 0 && houseNo.length == 0) {
        jQuery("#houseNoMsg").html("חובה להזין שדה זה");
        jQuery("#houseNoMsg").show();
        setErrorBox(jQuery('#houseNo'));
        retValue = false;
    } else if (jQuery('#houseNo').length == 0) {
        jQuery("#houseNoMsg").html("חובה להזין שדה זה");
        jQuery("#houseNoMsg").show();
        retValue = false;
    }

    if (errElm != null) {
        jQuery(window).scrollTop(errElm.position().top);
    }

    return retValue;
}

function validateName(name, nameMsgSelector, nameSelector, errElm, retValue) {
    if (name != null && name.length < 2) {
        jQuery("#" + nameMsgSelector).html("יש להזין שם המכיל לפחות שני תווים");
        jQuery("#" + nameMsgSelector).show();
        retValue = false;
        setErrorBox(jQuery('#' + nameSelector));
        if (errElm == null) {
            errElm = jQuery('#' + nameSelector);
        }
    } else if (nameValidate(name, jQuery("#" + nameMsgSelector), jQuery('#' + nameSelector))) {
        retValue = false;
        if (errElm == null) {
            errElm = jQuery('#' + nameSelector);
        }
    }
}

function validateID(idSelector, idMsgSelector) {
    var R_ELEGAL_INPUT = false;
    var R_NOT_VALID = false;
    var R_VALID = true;
    var str = jQuery('#' + idSelector).val();
    var IDnum = String(str);
    if ((IDnum.length > 9) || (IDnum.length < 4)) {
        jQuery('#' + idMsgSelector).html(' המספר צריך לכלול לפחות ארבעה תווים ');
        return R_ELEGAL_INPUT;
    }
    if (isNaN(IDnum)) {
        jQuery('#' + idMsgSelector).html('חובה להזין שדה זה');
        return R_ELEGAL_INPUT;
    }
    if (IDnum.length < 9) {
        while (IDnum.length < 9) {
            IDnum = '0' + IDnum;
        }
    }
    var mone = 0, incNum;
    for (var i = 0; i < 9; i++) {
        incNum = Number(IDnum.charAt(i));
        incNum *= (i % 2) + 1;
        if (incNum > 9)
            incNum -= 9;
        mone += incNum;
    }
    if (mone % 10 == 0)
        return R_VALID;
    else {
        jQuery('#' + idMsgSelector).html('מספר ת"ז אינו תקין');
        return R_NOT_VALID;
    }
}

function extDeviceShipmentStage(th,deviceType,packageName,price,customerType,ctnType) {
    googleAnalytics(deviceType,packageName,price,customerType,ctnType,'Address_Details');
    var parameters = jQuery('#checkNewBanForm3').serialize();
//    parameters = encodeURI(parameters);
    var url = '/SelfCare/PeleStoreConnectionNShepment.do?charset_heb_conv=utf-8&action=save&' + parameters + "&sendBillInMail=" + $('#mailcheck').attr('checked');

    var sessionEnd = "http://www.pelephone.co.il";
    jQuery.ajax({ type:"POST",
        url:url,
        context:document.body,
        beforeSend:function () {
            jQuery('#errMsg').hide();
            jQuery('#loadingImg4').show();
        },
        success:function (data) {
        },
        complete:function (jqXHR, textStatus) {
            var status = jqXHR.getResponseHeader('SESSION_STATUS');
            if (status == "ENDED") {
                window.location = sessionEnd;
            }
            var responseHeader = jqXHR.getResponseHeader('EAI_STATUS');
            var errMsgCode = jqXHR.getResponseHeader('errMsg');
            if (jQuery(th).closest(".contenSlid").prev('h2').find(".vIcon").is(":visible")) {
                var icon = jQuery(th).closest(".contenSlid").prev('h2').find(".vIcon");
                icon.text("עדכן");
                icon.addClass("upd");
            } else {
                jQuery(th).closest(".contenSlid").prev('h2').find(".vIcon").show();
            }
            loadPaymentPage(th);

            jQuery('#loadingImg4').hide();
        }});
}

function checkBillInMail(checkPionter) {
    if (jQuery(checkPionter).is(':checked')) {
        jQuery('#enviromentCare').hide();
        jQuery('.env').removeClass('envnoncare');
        jQuery('.env').addClass('envcare');
    } else {
        jQuery('#envCheck').attr('checked', true);
        jQuery('#enviromentCare').show();
        jQuery('.env').addClass('envnoncare');
        jQuery('.env').removeClass('envcare');

    }
}

function unCheckEnv() {
    jQuery('#mailcheck').attr('checked', true);
    jQuery('#enviromentCare').hide();
    jQuery('.env').removeClass('envnoncare');
    jQuery('.env').addClass('envcare');
}