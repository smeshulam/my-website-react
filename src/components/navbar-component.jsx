import React, { Component } from 'react'
import {NavLink } from 'react-router-dom'

export default class NavBar extends Component {
  render() {
    return (
        <nav>
            <ul>
                <li>
                    <NavLink to="/">About</NavLink>
                </li>
                <li>
                    <NavLink to="/portfolio">Portfolio</NavLink>
                </li>
            </ul>
        </nav>
    )
  }
}