// ------------------------------------------
// ---------- Shahar Meshulam test ----------
// ------------------------------------------
// for communit .
// ------------------------
; (function ($, window, document, undefined) {
    var pluginName = 'communit';

    function Plugin(element, options) {
        this.element = element;
        this._name = pluginName;
        this._dismissed = [];
        this._usersToShow = [];
        this._itemIndex = 0;
        this._defaults = $.fn.communit.defaults;
        this.options = $.extend({}, this._defaults, options);

        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        // Initialization logic
        init: function () {
            var THIS = this;
            // this is a "fake response" from the server
            ajaxResponse = JSON.parse(this.options.users);
            //get dismmised from local storage
            if (localStorage.getItem(this.options.type + 'Dismissed')) {
                this._dismissed = localStorage.getItem(this.options.type + 'Dismissed')
                this._dismissed = this._dismissed.split(',');
            }
            //grep users to show
            this._usersToShow = jQuery.grep(ajaxResponse, function (user) {
                return $.inArray(String(user.id), THIS._dismissed) == -1; //dissmised user
            });

            this.buildCache();

            //build list items
            if (this._usersToShow.length > 0) {
                for (var i = 0; i < this.options.numOfItemsToShow; i++) {
                    if (!this._usersToShow[i]) return;
                    this.bulidElement(this._usersToShow[i]);
                    this._itemIndex = i;
                }
            }
        },
        bulidElement: function (user) {
            var THIS = this;
            var userId = user.id;
            var html = $('<li class="list-group-item"><span class="list-close"></span><div class="table"><div class="table-cell"><a href="#"><span class="avatar avatar-big" style="background-image: url(' + user.profileImg + ')"></span></a></div><div class="table-cell"><h5>' + user.name + ' <span>@' + user.screen_name + '</span></h5><p><span>Following you</span><br /><span class="followers"><b>' + user.followers_count + '</b> Followers</span></p></div></div></li>');
            var fllowerBtns = $('<button class="btn btn-follow bg-main"><span></span>Follow</button><button class="btn btn-sayhi bg-main"><span></span>Say Hi</button>');
            var unFllowerBtns = $('<button class="btn btn-unfollow bg-gry"><span></span>Unfollow</button>');
            if (user.type != "none") {
                var type = $('<span class="profile-influncer">' + user.type + '</span>');
                html.find('p').prepend(type)
            }
            html.find('.list-close').click({ userId: user.id }, function () {
                this.closest('li').remove();
                THIS.dismiss.call(THIS, userId);
            });
            //attach onclick to the element buttons
            switch (this.options.type) {
                case "followers":
                    html.find(".table-cell:nth-child(2)").append(fllowerBtns);
                    html.find('.btn-follow').click({ userId: user.id }, function () {
                        this.closest('li').remove();
                        THIS.sendToServer("follow", userId);
                        THIS.dismiss.call(THIS, userId);
                    });
                    html.find('.btn-sayhi').click({ userId: user.id }, function () {
                        this.closest('li').remove();
                        THIS.sendToServer("sayhi", userId);
                        THIS.dismiss.call(THIS, userId);
                    });
                    break;
                case "unfollowers":
                    html.find(".table-cell:nth-child(2)").append(unFllowerBtns)
                    html.find('.btn-unfollow').click({ userId: user.id }, function () {
                        this.closest('li').remove();
                        THIS.sendToServer("unfollow",userId);
                        THIS.dismiss.call(THIS, userId);
                    });
                    break;
            }

            this.$element.append(html);
        },
        // add proflie to list
        add: function () {
            var nextIndex = this._itemIndex + 1;
            if (this._usersToShow[nextIndex]) {
                this._itemIndex++;
                this.bulidElement(this._usersToShow[this._itemIndex])
            }
        },
        // remove proflie from list
        dismiss: function (userId) {
            this._dismissed.push(String(userId))
            localStorage.setItem(this.options.type + 'Dismissed', this._dismissed);
            this.add();
        },
        sendToServer: function (action, userId) {
            var data = {}
            data.userId = userId;
            data.action = action;

            $.ajax({
                url: 'http://11.11.11.11',
                type: 'post', 
                data: data,
                dataType: 'json',
                success: function (data) {
                    // etc...
                }
            });
            
            alert("look in network tab to see the post");
        },
        buildCache: function () {
            this.$element = $(this.element);
        },
    });

    $.fn.communit = function (options) {
        this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
        return this;
    };

    $.fn.communit.defaults = {
        users: 'value',
        type: 'value',
        numOfItemsToShow: 3,
    };

})(jQuery, window, document);