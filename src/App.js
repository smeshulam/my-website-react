import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
//import logo from './logo.svg';
//import './App.css';
import Header from './components/header-component'
import Footer from './components/footer-component'
import NavBar from './components/navbar-component'
import Portfolio from './components/portfolio-component'
import About from './components/about-component'

function App() {
  return (
    <BrowserRouter>
      <div className="container">
      <Header/>
      <NavBar/>
      <section className="inner-content">           
        <Route exact path="/" component={About}/>
        <Route path="/portfolio" component={Portfolio}/>
      </section>
      <Footer/>
      </div>
    </BrowserRouter>
  );
}

export default App;
