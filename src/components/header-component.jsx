import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
        <header className="round shadow">
            <h1 id="site-title">Shahar Meshulam /&nbsp;<span>R&amp;D Web Development</span></h1>
            <p>Take a look at my code: <a href="https://bitbucket.org/smeshulam/" target="_blank">https://bitbucket.org/smeshulam/</a></p>
        </header>
    )
  }
}