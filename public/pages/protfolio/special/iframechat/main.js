//CHAT APP
window.addEventListener("load", init);

var room = document.getElementById('main-room').contentDocument || document.getElementById('main-room').document //the main iframe;
var chats = [];

var currElem;

var zIndex = 0;

var stateMouseDown = false;

function init() {
    //add style too room Iframe
    room.open();
    room.write(
        '<!DOCTYPE HTML >' +
        '<html><head><style>div{border:10px solid black;overflow:none;float:left;}</style><link href="style.css" rel="stylesheet" type="text/css"><\/head><body><\/body><\/html>'
    );
    room.close();

    //addEventListener for postMessage
    window.addEventListener("message",
      function (e) {
          respondToMessage(e);
      },
      false);
}

function createChat() {
    //create iframe element for a chat
    var div = document.createElement('div')
    var iframe = document.createElement('iframe');
    div.appendChild(iframe)
    currElem = div;
    chats.push(div);
    div.id = "iframe" + chats.length;
    //send joined room message
    div.addEventListener("DOMNodeInserted", function (e) {
        top.postMessage('iframe' + chats.length + ' joined the chat,' + div.id, '*');
        ele = e.target;
        
    }, true);
    //add drag events
    div.addEventListener("mousedown", eleMouseDown, false);
    //add iframe chat to room iframe
    room.body.appendChild(div);

    chatIframeDoc = iframe.contentDocument || iframe.contentWindow.document
    //set html for the chat iframe
    chatIframeDoc.open();
    chatIframeDoc.write( 
        '<!DOCTYPE HTML>' +
        '<html><head>' +
        '<link href="style.css" rel="stylesheet" type="text/css"><\/head><body id="'+"iframe" + chats.length +'"><\/body><\/html>'
    );
    chatIframeDoc.close();
    chatIframeDoc.body.innerHTML = '<div class="chat-title"><h1>Iframe' + chats.length + '</h1></div><input type="text"></input>' +
    '<button onclick="var target=this.parentElement;top.postMessage(document.getElementsByTagName(\'input\')[0].value + \', \' + target.id,\'*\');document.getElementsByTagName(\'input\')[0].value=\'\'">send</button>' +
    '<div id="textarea"><textarea readonly>you entered the chat</textarea></div>';
}



//handling posts events
function respondToMessage(postEvent) {
    for (var i = 0; i < chats.length; i++) {
        var data = postEvent.data.split(",");
        var massage = String(data[0]).trim();
        var eleId = String(data[1]).trim();
        if (chats[i].id != eleId) {
            var iframeContent = chats[i].getElementsByTagName('iframe')[0].contentDocument || chats[i].getElementsByTagName('iframe')[0].contentdocument;
            var textArea = iframeContent.getElementsByTagName('textarea')[0];
            textArea.value += "\n" + massage;
        }
    }

}

//DRAG AND DROP


//onMouseDown
function eleMouseDown(ev) {
    zIndex++;
    stateMouseDown = true;
    currElem = ev.currentTarget;
    document.getElementById('main-room').contentWindow.addEventListener("mousemove", eleMouseMove, false);
    document.getElementById('main-room').contentWindow.addEventListener("mouseup", eleMouseUp, false);
    for (var i = 0; i < chats.length; i++) {
        if (currElem != chats[i]) {
            chats[i].style.pointerEvents = "none";
        }
    }
}

//onDrag
function eleMouseMove(ev) {
    if (stateMouseDown) {
        var pX = ev.pageX;
        var pY = ev.pageY;
        currElem.style.position = "absolute";
        currElem.style.zIndex = zIndex;
        currElem.style.left = pX + "px";
        currElem.style.top = pY + "px";
    }
}

//onMouseUp
function eleMouseUp() {
    stateMouseDown = false
    for (var i = 0; i < chats.length; i++) {
        chats[i].style.pointerEvents = "all";
    }
    document.getElementById('main-room').contentWindow.removeEventListener("mousemove", eleMouseMove, false);
    document.getElementById('main-room').contentWindow.removeEventListener("mouseup", eleMouseUp, false);
}

